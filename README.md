# Edbit

Edbit (**E**ducational **D**ata**b**ase **I**nteracive **T**erminal) is a
program for executing SQL commands towards a PostgreSQL database.

Call

```
java -jar edbit.jar --help
```

for usage. Note that, in adition to all (Postgre)SQL-commands and queries,
Edbit also supports [Lore](https://gitlab.com/leifhka/lore)-commands and
queries if given the `-L` flag.
Edbit can also connect to a database via an SSH-connection
using the `--sshHost` and `--sshUser` flags.

## Keybindings and Layout

Edbit's UI consists of two text fields each enclosed in a square.
The bottom field is editable and used to write and execute SQL-queries
and commands. Once a command is executed (with `CTRL+ENTER`), the field
displays the result of the query, and is then non-editable. The executed
query is placed in the topmost field.
If `ENTER` is pressed, the results are moved to the topmost (history) field
and the bottom field is again empty and a new query can be added.

The top field shows the past commands (history) and the
results from each command (similary to any other shell).
The history will comment out results, and therefore
becomes a proper SQL-script (which can be copied into
a separate file thus becomming an executable SQL-file).

**Keybindings in edit mode:**

- `CTRL+ENTER`: Execute query (and enter view-mode)
- `CTRL+p` or `CTRL+UP`: Previous command in history
- `CTRL+n` or `CTRL+DOWN`: Next command in history
- `CTRL+SPACE`: Auto-complete schema, table or column name

**Keybindings in view mode:**

- `ENTER`: Exit view-mode and go back to edit-mode
- `W`: Open current query and result in new window
- `M`: Open current query and result in new monitored window (rerun on each command in original window)
- `SPACE`: Show more results/rows in case of more than 1000 rows
- `H, J, K, L` : Scroll left, down, up, right resp. (Hold shift to increase scroll-length)

**General Keybindings:**

- `CTRL+PLUS`: (`PLUS` is the `+`-key) Increase font size
- `CTRL+MINUS`: (`MINUS` is the `-`-key) Increase font size
- `CTRL+s`: Save history to file
- `CTRL+x`: Cut
- `CTRL+c`: Copy
- `CTRL+v`: Paste
- `CTRL+R`: Refresh available auto-complete schema, table or column names

**Commands:**

- `\d`: Displays all tables and views (in all schemas)
- `\d <table>`: Displays information about `<table>`
- `\dn`: Displays all schemas in database
- `\e <query>`: Opens up an external editor for writing query initialized with `<query>` (configurable with the `-x`-flag)
- `\i <file>`: Execute the commands in the file with path `<file>` (e.g. `\i init.sql`)

## Build

The project requires Java 11 and Maven. In adition,
the project depends on [Lore](https://gitlab.com/leifhka/lore) (for Lore-mode).
As this project is not (yet) on Maven Central,
one needs to clone Lore, build it (with `sbt`, Lore is written in Scala),
and publish it with Maven (locally) with

```
git clone https://gitlab.com/leifhka/lore
cd lore
sbt assembly
sh publishToMaven.sh
```

Then, Edbit is built by simply executing

```
mvn install
```

The executable can be found at `target/edbit.jar`.
