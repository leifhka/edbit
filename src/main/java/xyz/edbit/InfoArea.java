package xyz.edbit;

import java.util.Collection;
import java.util.Collections;

import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.model.StyleSpans;
import org.fxmisc.richtext.model.StyleSpansBuilder;

public class InfoArea extends CodeArea {

    private final String keyword;
    private final String value;

    public InfoArea(String keyword, String value) {

        this.keyword = keyword + ":";
        this.value = value;
        this.setId("infoArea");
        this.appendText(this.keyword + " " + this.value);
        setStyleSpans(0, computeHighlighting());
    }

    private StyleSpans<Collection<String>> computeHighlighting() {

        StyleSpansBuilder<Collection<String>> spansBuilder = new StyleSpansBuilder<>();
        spansBuilder.add(Collections.singleton("keyword"), this.keyword.length());
        return spansBuilder.create();
    }
}
