package xyz.edbit;

import java.sql.ResultSet; 
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types; 

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

public class Table {

    private static final Set<Integer> rightAdjustedColumns = Set.of(
            Types.BIGINT, Types.DECIMAL, Types.DOUBLE, Types.FLOAT, 
            Types.INTEGER, Types.NUMERIC, Types.REAL, Types.SMALLINT, Types.TINYINT);

    public static int ROWS_IN_MEMORY = 1000;
    private final ResultSet resultSet;
    private boolean hasMoreRows;
    private List<List<String>> rows;
    private List<Column> columns;
    private int nrColumns;

    public Table(ResultSet resultSet) {
        this.resultSet = resultSet;
        this.hasMoreRows = true;
    }

    public boolean hasMoreRows() {
        return this.hasMoreRows;
    }

    public void populate() throws SQLException {

        addColumns();
        addRows();
    }

    private void addRows() throws SQLException {

        this.rows = new ArrayList<List<String>>();

        while (this.rows.size() < ROWS_IN_MEMORY) {
            if (this.resultSet.next()) {
                this.rows.addAll(addRow());
            } else {
                this.hasMoreRows = false;
                return;
            }
        }
    }

    private void addColumns() throws SQLException {

        ResultSetMetaData metaData = this.resultSet.getMetaData();
        this.columns = new ArrayList<Column>();
        this.nrColumns = metaData.getColumnCount();

        for (int i = 1; i <= nrColumns; i++) {
            String name = metaData.getColumnName(i);
            int type = metaData.getColumnType(i);
            this.columns.add(new Column(i-1, name, type));
        }
    }

    private List<List<String>> addRow() throws SQLException {

        List<String> row = new ArrayList<>();

        for (int i = 1; i <= nrColumns; i++) {
            String val = this.resultSet.getString(i);
            val = val == null ? "" : val; // TODO: Handle null better
            row.add(val); 
            updateColumn(i-1, val);
        }
        return splitOnNewlines(row); 
    }

    private List<List<String>> splitOnNewlines(List<String> row) {
        
        List<List<String>> rowLines = new ArrayList<>();
        boolean containsNewline = false;

        for (String val : row) {

            List<String> valLines = val.lines()
                .collect(Collectors.toList());

            if (valLines.size() > 1) {
                containsNewline = true;
                rowLines.add(valLines);
            } else {
                rowLines.add(List.of(val));
            }
        }
        
        if (containsNewline) {
            return zipLines(rowLines);
        } else {
            return List.of(row);
        }
    }

    private List<List<String>> zipLines(List<List<String>> lines) {
        
        int numLines = lines.stream()
            .mapToInt(l -> l.size())
            .max()
            .orElse(0);

        List<List<String>> rows = new ArrayList<>();

        for (int i = 0; i < numLines; i++) {
            List<String> row = new ArrayList<>();
            for (int c = 0; c < lines.size(); c++) {

                if (lines.get(c).size() > i + 1) { // Add + at end to signal continuation
                    row.add(addPlusAtEnd(lines.get(c).get(i), this.columns.get(c).getWidth()));
                } else if (lines.get(c).size() == i + 1) { // Final line
                    row.add(lines.get(c).get(i));
                } else {
                    row.add(""); // Empty line
                }
            }
            rows.add(row);
        }
        return rows;
    }

    private String addPlusAtEnd(String line, int width) {
        return line + " ".repeat(width - line.length() - 1) + "+";
    }

    private void updateColumn(int index, String val) {

        int width = val.lines()
            .mapToInt(l -> l.length())
            .max()
            .orElse(0);

        this.columns.get(index).updateMinWidth(width);
    }

    public int print(SqlCodeArea textArea) {
        printColumns(textArea);
        return printRows(textArea);
    }

    private void printColumns(SqlCodeArea textArea) {
    
        String sep = "";
        int col = 0;
        StringBuilder out = new StringBuilder();

        for (int i = 0; i < this.nrColumns; i++) {
            Column c = this.columns.get(i);
            String r = sep + StringUtils.center(c.getName(), c.getWidth());
            out.append(r); 
            col += r.length();
            sep = " | ";
        }
        out.append("\n");

        for (int i = 0; i < col; i++) {
            out.append("-");
        }
        out.append("\n");
        textArea.appendText(out.toString());
    }

    private int printRows(SqlCodeArea textArea) {

        StringBuilder out = new StringBuilder();

        for (List<String> row : this.rows) {
            String sep = "";
            for (int i = 0; i < this.nrColumns; i++) {
                Column col = this.columns.get(i);
                String adjust = col.isLeftAdjusted() ? "-" : "";
                String r = sep + String.format("%" + adjust + col.getWidth() + "s", row.get(i));
                out.append(r);
                sep = " | ";
            }
            out.append("\n");
        }
        textArea.appendText(out.toString());
        return this.rows.size();
    }

    public int printMoreRows(SqlCodeArea textArea) throws SQLException {
        addRows();
        return printRows(textArea);
    }

    public void close() throws SQLException {
        this.resultSet.close();
    }

    private class Column {

        private final int index;
        private final String name;
        private final int type;
        private int width;

        public Column(int index, String name, int type) {
            this.index = index;
            this.name = name;
            this.type = type;
            this.width = name.length();
        }

        public void updateMinWidth(int newWidth) {
            this.width = Math.max(this.width, newWidth);
        }

        public int getWidth() {
            return this.width;
        }

        public String getName() {
            return this.name;
        }

        public int getIndex() {
            return this.index;
        }
        
        public boolean isLeftAdjusted() {
            return !rightAdjustedColumns.contains(this.type);
        }
    }
}

