package xyz.edbit;

import java.io.IOException;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Map;
import java.util.TreeSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.ScrollEvent;

public class EditableSqlCodeArea extends SqlCodeArea implements EventHandler<KeyEvent> {

    private ExecutorService executor;
    private final Stage primaryStage;
    private final Connection connection;
    private final HistorySqlCodeArea history;
    private final Configuration configuration;
    private final boolean debugMode;
    private IterableQueryOutput currentResults; 
    private final Map<SqlCodeArea, String> monitorWindows;

    private final List<String> commandHistory;
    private int historyIndex = -1;
    private Path saveFilePath;

    public EditableSqlCodeArea(Stage primaryStage, Connection connection, Configuration configuration, MetaDatabase metaDatabase, HistorySqlCodeArea history, boolean debugMode) {

        super(metaDatabase);
        this.primaryStage = primaryStage;
        this.executor = Executors.newSingleThreadExecutor();
        this.connection = connection;
        this.configuration = configuration;
        this.debugMode = debugMode;
        this.history = history;
        this.commandHistory = new LinkedList<>();
        this.monitorWindows = new HashMap<>();

        setEditable(true);
        setOnKeyPressed(this);
        try {
            this.connection.setAutoCommit(false);
        } catch (SQLException ex) {
            System.err.println("ERROR: " + ex.getMessage());
        }
    }

    public void handle(KeyEvent event) {

        if (!isEditable() && handleShowingResultsEvent(event)) {
            return;
        } else if (isEditable() && handleEditTextEvent(event)) {
            return;
        } else {
            handleGenericEvent(event);
        }
    }

    private String getIndentOfCurrentLine() {
        int caretPos = getCaretPosition();
        String[] lines = getText().substring(0, caretPos).split("\\n");
        if (lines.length == 0) {
            return "";
        }
        Matcher matcher = Pattern.compile("^\\s*").matcher(lines[lines.length - 1]);
        matcher.find();
        return matcher.group();
    }

    private boolean handleEditTextEvent(KeyEvent event) {

        // Fix for dead tilde-key not regognized (is undefined)
        // This is an open bug in OpenJDK, more info at:
        // https://bugs.openjdk.java.net/browse/JDK-8183521
        if (event.getCode() == KeyCode.UNDEFINED) {
            if (event.isAltDown()) {
                appendText("~");
                return true;
            } else if (event.isShiftDown()) {
                appendText("^");
                return true;
            }
            return false;
        }

        if (!event.isShortcutDown() && event.getCode() == KeyCode.ENTER) {
            // Add indent equal to this line's
            String toInsert = getIndentOfCurrentLine();
            insertText(getCaretPosition(), toInsert);
            return true;
        }

        if (!event.isShortcutDown()) {
            return false; // All commands below require this
        }

        if (event.getCode() == KeyCode.ENTER) {

            handleExecuteQueryCommand();
            return true;

        } else if (event.getCode() == KeyCode.SPACE) {

            handleAutoCompleteCommand();
            return true;

        } else if (this.commandHistory.size() > (1 + this.historyIndex) &&
                   (event.getCode() == KeyCode.UP || event.getCode() == KeyCode.P)) {

            handlePreviousCommand();
            return true;

        } else if (this.historyIndex >= 0 &&
                   (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.N)) {

            handleNextCommand();
            return true;

        } else if (event.getCode() == KeyCode.S) {

            handleSaveFileCommand();
            return true;
        } else if (event.getCode() == KeyCode.R) {

            handleRefreshMetaDatabaseCommand();
            return true;
        }

        return false;
    }

    private void handleAutoCompleteCommand() {

        int caretPos = getCaretPosition();
        String[] words = getText().substring(0, caretPos).split("\\s");
        String currentWord = words[words.length-1].strip();
        if (currentWord.isEmpty()) {
            return;
        }

        Set<String> matching = new HashSet<>();
        String longestCommonPrefix = null;

        for (String name : getMetaDatabase().getAllNames()) {
            if (name.startsWith(currentWord)) {
                matching.add(name);
                longestCommonPrefix = longestCommonPrefix == null
                    ? name
                    : getLongestCommonPrefix(longestCommonPrefix, name);
            }
        }

        if (matching.isEmpty()) {
            return; // No match -> Nothing to complete to
        }

        if (!longestCommonPrefix.equals(currentWord)) {
            String toInsert = longestCommonPrefix.substring(currentWord.length());
            insertText(caretPos, toInsert);
            return;
        } else {
            // Multiple possible, print options to history (like a normal shell)
            // but skip all with a dot (".") in the completed part
            matching = matching.stream()
                .filter(n -> !n.matches(currentWord + ".*\\..*"))
                .collect(Collectors.toSet());
            matching = new TreeSet<String>(matching); // Sort alphabetically
            this.history.appendInfo(String.join(" ", matching));
        }
    }

    // Does not work, as getWidth() returns pixel-width, not letter/column width
    //private String makePrintableLines(Set<String> words) {

    //    double maxLineLen = this.history.getWidth();
    //    System.err.println("MAXLEN: " + maxLineLen);
    //    StringBuilder out = new StringBuilder();
    //    String sep = "";
    //    double lineLen = 0;

    //    for (String word : new TreeSet<String>(words)) { // Sort alphabetically
    //        if (lineLen + word.length() + 4 > maxLineLen) { // 4 = "-- " + space
    //            sep = "\n";
    //            lineLen = 0;
    //        }
    //        out.append(sep + word);
    //        sep = " ";
    //    }
    //    return out.toString();
    //}

    private String getLongestCommonPrefix(String str1, String str2) {

        int shortest = Math.min(str1.length(), str2.length());

        if (shortest == 0) {
            return "";
        }
        
        int i = 0;
        for (; i < shortest; i++) {
            if (str1.charAt(i) != str2.charAt(i)) {
                break;
            }
        }
        return str1.substring(0, i);
    }

    private void handleSaveFileCommand() {

        if (this.saveFilePath == null) {
            chooseSaveFile();
        }

        if (this.saveFilePath != null) {
            saveFile();
        }
    }

    private void handleRefreshMetaDatabaseCommand() {

        try {
            getMetaDatabase().initMetadata(this.connection);
        } catch (SQLException ex) {
            setMessage("ERROR: Error while querying database for available names for auto-complete: " + ex.getMessage());
            return;
        }
        setMessage("Auto-complete names refreshed!");
    }

    private void handleNextCommand() {
        
        clear();
        this.historyIndex--;
        if (this.historyIndex >= 0) {
            appendText(this.commandHistory.get(this.historyIndex));
        }
    }

    private void handlePreviousCommand() {

        clear();
        this.historyIndex++;
        appendText(this.commandHistory.get(this.historyIndex));
    }

    private void handleExecuteQueryCommand() {

        String query = getText();

        addToHistory(query);
        addToCommandHistory(query);

        clear();
        super.stopHighlighting(); // Stop syntax-highlighting for results
        //appendText("\n");

        runQuery(query);
        setEditable(false);
    }

    private void addToHistory(String query) {

        // Comment-out \-commands before adding to history
        String historyQuery = query.startsWith("\\")
            ? "-- " + query
            : query;

        this.history.appendQuery(historyQuery);
    }

    private void chooseSaveFile() {

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose where to save file");
        fileChooser.getExtensionFilters().addAll(
                new ExtensionFilter("SQL Files", "*.sql"),
                new ExtensionFilter("Text Files", "*.txt"),
                new ExtensionFilter("All Files", "*.*"));
        File f = fileChooser.showSaveDialog(this.primaryStage);
        this.saveFilePath = f == null ? null : f.toPath();
    }

    private void saveFile() {

        String toSave = this.history.getText();
        try {
            Files.writeString(this.saveFilePath, toSave);
        } catch (IOException ex) {
            String msg = "Error writing file: " + ex.getMessage();
            setMessage(msg);
            this.saveFilePath = null;
            return;
        }
        String msg = "History saved to " + this.saveFilePath.toString();
        setMessage(msg);
    }

    private void setMessage(String msg) {
        clear();
        appendText("\n" + msg);
        setEditable(false);
    }

    private boolean handleShowingResultsEvent(KeyEvent event) {

        if (event.getCode() == KeyCode.ENTER) {

            if (this.currentResults != null) { // Showing results from query
                this.history.appendResults(getText());
                this.currentResults.close();
                this.currentResults = null;
            } // else showing info/error message

            clear();
            super.startHighlighting();
            setEditable(true);
            return true;

        } else if (event.getCode() == KeyCode.SPACE && this.currentResults != null) {

            this.currentResults.executeAndPrintResults();
            return true;

        } else if (event.getCode() == KeyCode.W) {
            
            handleOpenResultWindow();
            return true;
        } else if (event.getCode() == KeyCode.M) {
            
            handleOpenMonitorWindow();
            return true;
        } else if (event.getCode() == KeyCode.J) {
            scrollView(true, event.isShiftDown(), 1);
            return true;
        } else if (event.getCode() == KeyCode.K) {
            scrollView(true, event.isShiftDown(), -1);
            return true;
        } else if (event.getCode() == KeyCode.H) {
            scrollView(false, event.isShiftDown(), -1);
            return true;
        } else if (event.getCode() == KeyCode.L) {
            scrollView(false, event.isShiftDown(), 1);
            return true;
        }

        return false;
    }

    private void scrollView(boolean yDir, boolean longScroll, int positive) {

        double scrollLength = positive * (longScroll ? this.getHeight() : 10);

        if (yDir) {
            this.scrollYBy(scrollLength);
        } else {
            this.scrollXBy(scrollLength);
        }
    }

    private void handleOpenResultWindow() {

        HistorySqlCodeArea newHistory = new HistorySqlCodeArea(getMetaDatabase());
        newHistory.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

        Scene scene = new Scene(newHistory, 800, 500);
        scene.getStylesheets()
            .add(SqlCodeArea.class.getClassLoader()
                            .getResource("style.css")
                            .toExternalForm());

        Stage stage = new Stage();
        stage.setTitle("Edbit: Results");
        stage.setScene(scene);
        stage.show();

        newHistory.addQuery(this.commandHistory.get(0));
        newHistory.addResults(getText());
    }

    private void handleOpenMonitorWindow() {

        SqlCodeArea newArea = new SqlCodeArea(getMetaDatabase());
        newArea.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

        String query = this.commandHistory.get(0);
        this.monitorWindows.put(newArea, query);

        Scene scene = new Scene(newArea, 800, 500);
        scene.getStylesheets()
            .add(SqlCodeArea.class.getClassLoader()
                            .getResource("style.css")
                            .toExternalForm());

        Stage stage = new Stage();
        stage.setTitle("Edbit: Monitor query");
        stage.setScene(scene);
        stage.setOnCloseRequest(e -> monitorWindows.remove(newArea));
        stage.show();

        refreshMonitorWindow(newArea, query);
    }

    private void refreshMonitorWindows() {
        for (var historyQuery : this.monitorWindows.entrySet()) {
            refreshMonitorWindow(historyQuery.getKey(), historyQuery.getValue());
        }
    }

    private void refreshMonitorWindow(SqlCodeArea area, String query) {
        try {
            area.clear();

            String expandedQuery = configuration.expandCommand(query)
                .toSingleQuery().trim();
            area.appendText(expandedQuery + "\n\n");
            Statement stmt = connection.createStatement();
            ResultSet res = stmt.executeQuery(expandedQuery);
            Table table = new Table(res);
            table.populate();
            table.print(area);

        } catch (Exception ex) {
            setMessage("ERROR: Executing monitor query:\n"
                    + query + "\nfailed with message: " + ex.getMessage());
            return;
        }
    }

    private boolean handleGenericEvent(KeyEvent event) {

        if (!event.isShortcutDown()) {
            return false; // All commands below require this
        }
            
        if (event.getCode() == KeyCode.PLUS) {
            increaseFontSize(); 
            history.increaseFontSize();
            return true;
        } else if (event.getCode() == KeyCode.MINUS) {
            decreaseFontSize(); 
            history.decreaseFontSize();
            return true;
        }

        return false;
    }

    private void addToCommandHistory(String query) {

        this.historyIndex = -1;
        if (this.commandHistory.isEmpty() || !this.commandHistory.get(0).equals(query)) {
            this.commandHistory.add(0, query); // Do not add duplicates
        }
    }

    private void runQuery(String query) {

        ExpandedQuery expandedQuery = null;
        try {
            expandedQuery = configuration.expandCommand(query);
        } catch (Exception ex) {
            setMessage("ERROR: Expanding query failed with input query:\n"
                    + query + "\nwith message: " + ex.getMessage());
            return;
        }

        if (expandedQuery.isUsermadeQuery()) { // Add to history
            String expandedQueryStr = expandedQuery.toSingleQuery();
            this.history.appendQuery(expandedQueryStr);
            addToCommandHistory(expandedQueryStr);
        }

        this.currentResults = new IterableQueryOutput(expandedQuery.getQueries().iterator(), expandedQuery.isCommandQuery());
        this.currentResults.executeAndPrintResults();

        refreshMonitorWindows();
    }

    private class IterableQueryOutput {

        private final Iterator<String> queries;
        private final boolean commandQuery; // From \-command

        private Table currentTable;
        private int totalRows;
        private Statement stmt;
        private String resultSeparator = ""; // Set to "\n" after first query

        public IterableQueryOutput(Iterator<String> queries, boolean commandQuery) {
            this.queries = queries;
            this.commandQuery = commandQuery;
            this.totalRows = 0;
            try {
                this.stmt = connection.createStatement();
            } catch (SQLException ex) {
                setMessage("\n" + ex.getMessage() + "\n");
            }
        }

        public boolean currentTableHasMoreRows() {
            return this.currentTable != null && this.currentTable.hasMoreRows();
        }

        public boolean hasMoreRowsOrQueries() {
            return currentTableHasMoreRows() || this.queries.hasNext();
        }

        public void executeAndPrintResults() {

            if (!hasMoreRowsOrQueries()) {
                return;
            }

            try {
                boolean containsResults = true;

                if (this.currentTable != null) { // Fetch more results from currentTable

                    this.totalRows += this.currentTable.printMoreRows(EditableSqlCodeArea.this);

                } else {                         // Execute new query

                    String query = this.queries.next();
                    containsResults = executeQuery(query);
                    printResultOutput(query, containsResults);
                }

                if (!currentTableHasMoreRows()) { // No more results here, go to next query
                    if (!this.commandQuery && containsResults) {
                        // Print total number of rows outputted
                        String plurality = this.totalRows != 1 ? "s" : "";
                        appendText("(" + this.totalRows + " row" + plurality + ")");
                    }

                    closeCurrentTable();
                    executeAndPrintResults();
                }
                connection.commit();
            } catch (SQLException ex) {
                setMessage("ERROR: " + ex.getMessage() + "\n\n");
                try {
                    connection.rollback();
                } catch (SQLException ex2) {
                    setMessage("ERROR when attempting rollback: " + ex2.getMessage() + "\n\n");
                }
            }
        }

        private boolean executeQuery(String query) throws SQLException {

            if (debugMode) {
                System.err.println("[DEBUG] Executing:\n" + query);
            }
            return this.stmt.execute(query);
        }

        private void printResultOutput(String query, boolean containsResults) throws SQLException {

            appendText(this.resultSeparator);

            if (!containsResults) { // Non-SELECT-command (e.g. INSERT, CREATE)
                printCommandInfo(query);
            } else {
                printQueryResults();
            }
        }

        private void printQueryResults() throws SQLException {

            ResultSet res = this.stmt.getResultSet();
            this.currentTable = new Table(res);
            this.currentTable.populate();
            this.totalRows += this.currentTable.print(EditableSqlCodeArea.this);

            this.resultSeparator = "\n";
        }

        private void printCommandInfo(String query) throws SQLException {

            String command = query.trim().split(" ", 2)[0]; // Get command name (INSERT, CREATE, etc.)
            String msg = command + " done.";
            if (this.stmt.getUpdateCount() > 0) {
                msg += " Inserted/updated " + this.stmt.getUpdateCount();
                msg += this.stmt.getUpdateCount() > 1 ? " rows." : " row.";
            }
            appendText(msg + "\n\n");
        }

        private void closeCurrentTable() throws SQLException {

            if (this.currentTable != null) {
                this.currentTable.close();
            }
            this.totalRows = 0;
            this.currentTable = null; 
        }

        public void close() {
            try {
                this.stmt.close();
            } catch (SQLException ex) {
                setMessage("ERROR: " + ex.getMessage() + "\n");
            }
        }
    }
}
