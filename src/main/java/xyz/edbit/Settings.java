package xyz.edbit;

import picocli.CommandLine.Command;
import picocli.CommandLine.IVersionProvider;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

@Command(
    name = "edbit",
    descriptionHeading = "%n@|bold DESCRIPTION:|@%n",
    parameterListHeading = "%n@|bold PARAMETERS:|@%n",
    optionListHeading = "%n@|bold OPTIONS:|@%n",
    footerHeading = "%n",
    description = "Edbit (Educational DataBase Interactive Terminal) is a database shell (similar to psql) that can connect to PostgreSQL databases and execute queries and other SQL-commands, and display the results. For more information, keybindings and supported commands, see the full README at%n%n\thttps://gitlab.com/leifhka/edbit",
    footer = "@|bold EXAMPLES:|@%n\tjava -jar edbit.jar -d mydb -U myuser -h myhost -x vim",
    //mixinStandardHelpOptions = true, 
    versionProvider = Settings.JarFileVersionProvider.class)
public class Settings {

    @Option(names = { "-V", "--version" }, versionHelp = true,
            description = "print version information and exit")
    public boolean versionRequested;

    @Option(names = {"--help"}, usageHelp = true,
        description = {"Display this help message."})
    public boolean usageHelpRequested;

    @Option(names = {"-x", "--external"},
        description = {"Command to execute to edit query in external editor. Note: the path to the file to edit will be appended after this command."})
    public String external = "emacs";

    @Option(names = {"-d", "--database"},
        description = {"Which database to connect to."})
    public String database;

    @Option(names = {"-h", "--host"},
        description = {"The host containing the database to connect to."})
    public String host = "localhost";

    @Option(names = {"-U", "--user"},
        description = {"The username of the user to log in as."})
    public String username;

    @Option(names = {"-L", "--lore"},
        description = {"Allow Lore-statements in adition to SQL."})
    public boolean loreMode;

    @Option(names = {"--sshHost"},
        description = {"The SSH-host to tunnel the JDBC connection through."})
    public String sshHost;

    @Option(names = {"--sshUser"},
        description = {"The SSH-username to the SSH-connection. Defaults to the same as the database user."})
    public String sshUser;

    @Option(names = {"--debug"},
        description = {"Prints all executed queries (as sent to DB, i.e. expanded) to standard error."})
    public boolean debugMode;

    /**
     * This gets the version from the pom.xml file. Works only for jar file.
     */
    static class JarFileVersionProvider implements IVersionProvider {
        
        public String[] getVersion() {
            return new String[] { Settings.class.getPackage().getImplementationVersion() };
        }
    }

}

