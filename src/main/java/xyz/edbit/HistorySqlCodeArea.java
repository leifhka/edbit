/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.edbit;

import java.util.stream.Collectors;

/**
 *
 * @author leifhka
 */
public class HistorySqlCodeArea extends SqlCodeArea {

    private boolean empty = true;

    public HistorySqlCodeArea(MetaDatabase metaDatabase) {
        super(metaDatabase);
    }

    @Override
    public void appendText(String text) {

        if (!this.empty) {
            super.appendText("\n\n");
        }
        super.appendText(text);
        this.empty = false;
    }

    public void addQuery(String query) {
        this.appendText(query);
    }

    /**
     * Same as #addQuery, but also scrolls view to bottom
     */
    public void appendQuery(String query) {
        addQuery(query);
        this.scrollYBy(Double.MAX_VALUE); // Scrolls the pane to bottom
    }

    public void addResults(String result) {
        String commentedOutResult = result.lines().collect(Collectors.joining("\n-- "));
        this.appendText("-- " + commentedOutResult);
    }

    /** 
     * Same as #addResults, but also scrolls view 1 screen.
     * Used for adding results from queries, so the query and results are
     * both visible on the same screen.
     */
    public void appendResults(String result) {
        double scrollY = Math.min(this.getHeight(), this.getTotalHeightEstimate());
        addResults(result);
        this.scrollYBy(scrollY*0.95); // Scrolls the pane so that results starts at top of window (multiply by 0.95 to get some space on top)
    }

    /**
     * Same as #appendResults but scrolls view to bottom instead of just 1 screen.
     * Used for e.g. showing auto-complete alternatives.
     */
    public void appendInfo(String info) {
        addResults(info);
        this.scrollYBy(Double.MAX_VALUE); // Scrolls the pane to bottom
    }
}
