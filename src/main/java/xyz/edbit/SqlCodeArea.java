package xyz.edbit;

import java.time.Duration;
import java.util.Collection;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleDoubleProperty;

import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.model.StyleSpans;
import org.fxmisc.richtext.model.StyleSpansBuilder;
import org.reactfx.Subscription;

public class SqlCodeArea extends CodeArea {

    private static final String[] KEYWORDS = new String[] {
        "and", "as", "asc", "all",
        "between", "by", "btree",
        "case", "cascade", "create", "cross", 
        "delete", "desc", "drop", "distinct",
        "false", "forward", "from", "full", "function",
        "group", "gist",
        "hash", "having",
        "else", "end", "except", "exists",
        "implication", "import", "in", "index", "inner", "insert", "into", "if", "is", "intersect",
        "join",
        "left", "limit", "full", "lateral",
        "natural", "not", "null", 
        "on", "or", "order", "outer", "owned", "offset",
        "right", "relation", "role", "replace", "recursive",
        "select", "schema",
        "table", "then", "true",
        "unique", "update", "user", "using", "union", 
        "values", 
        "when", "where", "with", "view"
    };

    private static final String[] TYPES = new String[] {
        "any", "anyelement", "anyarray", "anynonarray", "anyenum", "anyrange", 
        "bigint", "bigserial", "bit", "boolean", "box", "bytea",
        "char", "character", "cidr", "circle",
        "date", "datetime", "daterange",
        "decimal", "double",
        "float", "float4", "float8",
        "geometry",
        "inet", "int", "integer", "interval", "int4range", "int8range",
        "json", "jsonb",
        "line", "linestring", "lseg",
        "macaddr", "money", "multipoint", "multiline", "multipolygon",
        "numeric", "numrange",
        "oid",
        "path", "point", "precision",
        "real", "regproc", "regprocedure", "regclass", "regoper", "regoperator", "regtype", "regrole", "regnamespace", "regconfig", "regdictionary", "record",
        "serial", "smallint", "smallserial",
        "text", "time", "timestamp", "timezone", "trigger", "tsvector", "tsquery", "tsrange", "tstzrange",
        "uuid",
        "varchar", "varying", "void",
        "without",
        "xml",
        "zone"
    };

    private static final String KEYWORD_PATTERN = "\\b(" + String.join("|", KEYWORDS) + ")\\b";
    private static final String TYPE_PATTERN = "\\b(" + String.join("|", TYPES) + ")\\b";
    private static final String PAREN_PATTERN = "\\(|\\)";
    private static final String SEMICOLON_PATTERN = "\\;";
    private static final String STRING_PATTERN = "'([^'])*'";
    private static final String COMMENT_PATTERN = "--[^\n]*";

    private final MetaDatabase metaDatabase;
    private Pattern pattern;
    private Subscription changeSubsctiption;
    private boolean syntaxHighlightingEnabled;
    private int fontSize = 15;

    public SqlCodeArea(MetaDatabase metaDatabase) { // Used for histories, so should only show results

        this.metaDatabase = metaDatabase;
        setEditable(false);
        this.pattern = makePattern(metaDatabase);
        this.syntaxHighlightingEnabled = true;
        makeChangeSubscription();
    }

    private static Pattern makePattern(MetaDatabase metaDatabase) {

        String dbNames = String.join("|", metaDatabase.getAllNames())
            .replace(".", "\\.");

        return Pattern.compile(
            "(?<KEYWORD>" + KEYWORD_PATTERN + ")"
            + "|(?<TYPE>" + TYPE_PATTERN + ")"
            + "|(?<PAREN>" + PAREN_PATTERN + ")"
            + "|(?<SEMICOLON>" + SEMICOLON_PATTERN + ")"
            + "|(?<STRING>" + STRING_PATTERN + ")"
            + "|(?<COMMENT>" + COMMENT_PATTERN + ")"
            + "|(?<DBNAME>(?!\\.)\\b(" + dbNames + ")\\b(?!\\.))" //no-dot, word-start, name, word-end, no-dot
        );
    }

    public MetaDatabase getMetaDatabase() {
        return this.metaDatabase;
    }

    public void increaseFontSize() {

        this.fontSize++;
        var fontSize = new SimpleDoubleProperty(this.fontSize); // font size in pt
        this.styleProperty().bind(Bindings.format("-fx-font-size: %.2fpt;", fontSize));
    }

    public void decreaseFontSize() {

        this.fontSize--;
        var fontSize = new SimpleDoubleProperty(this.fontSize); // font size in pt
        this.styleProperty().bind(Bindings.format("-fx-font-size: %.2fpt;", fontSize));
    }

    private void makeChangeSubscription() {
        this.setId("codeArea");
        this.changeSubsctiption = this.multiPlainChanges()
                .successionEnds(Duration.ofMillis(50))
                .subscribe(ignore -> setStyleSpans(0, computeHighlighting(getText())));
    }

    public void stopHighlighting() {
        this.syntaxHighlightingEnabled = false;
    }

    public void startHighlighting() {
        this.syntaxHighlightingEnabled = true;
    }

    public void cleanup() {
        this.changeSubsctiption.unsubscribe();
    }

    private StyleSpans<Collection<String>> computeHighlighting(String text) {

        StyleSpansBuilder<Collection<String>> spansBuilder = new StyleSpansBuilder<>();

        if (!this.syntaxHighlightingEnabled) {
            spansBuilder.add(Collections.emptySet(), text.length());
            return spansBuilder.create();
        }

        Matcher matcher = this.pattern.matcher(text.toLowerCase());
        int lastKwEnd = 0;

        while(matcher.find()) {
            String styleClass =
                    matcher.group("KEYWORD") != null ? "keyword" :
                    matcher.group("TYPE") != null ? "type" :
                    matcher.group("PAREN") != null ? "paren" :
                    matcher.group("SEMICOLON") != null ? "semicolon" :
                    matcher.group("STRING") != null ? "string" :
                    matcher.group("COMMENT") != null ? "comment" :
                    matcher.group("DBNAME") != null ? "dbname" :
                    null; /* never happens */ assert styleClass != null;
            spansBuilder.add(Collections.emptySet(), matcher.start() - lastKwEnd);
            spansBuilder.add(Collections.singleton(styleClass), matcher.end() - matcher.start());
            lastKwEnd = matcher.end();
        }
        spansBuilder.add(Collections.emptySet(), text.length() - lastKwEnd);
        return spansBuilder.create();
    }
}
