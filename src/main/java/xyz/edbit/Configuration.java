package xyz.edbit;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.leifhka.lore.DatabaseContext;
import org.leifhka.lore.Lore;

public class Configuration {

    private static final String TMP_FILENAME = "edbit_tmp";

    private final Map<String, Function<String, ExpandedQuery>> commands = new HashMap<>();
     
    private DatabaseContext dbContext;
    private String externalEditor;

    public Configuration(DatabaseContext dbContext, String externalEditor) {
        this.dbContext = dbContext;
        this.externalEditor = externalEditor;
        populateCommandsMap();
    }

    public Configuration(String externalEditor) {
        this(null, externalEditor);
    }

    private void populateCommandsMap() {

        this.commands.put("d", arg -> {
                if (arg.isEmpty()) {
                    return ExpandedQuery.command(
                        "WITH tables(schema, name, owner, table_type) AS (SELECT schemaname, tablename, tableowner, 'table' FROM pg_catalog.pg_tables WHERE schemaname NOT IN ('pg_catalog', 'information_schema')), views(schema, name, owner, table_type) AS (SELECT schemaname, viewname, viewowner, 'view' FROM pg_catalog.pg_views WHERE schemaname NOT IN ('pg_catalog', 'information_schema')) SELECT * FROM (SELECT * FROM tables UNION ALL SELECT * FROM views) t ORDER BY schema, name;");
                } else {
                    return ExpandedQuery.command(
                        String.format(
                            "SELECT a.attname AS column, pg_catalog.format_type(a.atttypid, a.atttypmod) AS type, CASE WHEN a.attnotnull THEN 'not null' ELSE NULL END AS nullable, pg_catalog.pg_get_expr(d.adbin, d.adrelid, true) AS default FROM pg_catalog.pg_attribute AS a LEFT JOIN pg_catalog.pg_attrdef AS d ON (a.attrelid = d.adrelid AND a.attnum = d.adnum) WHERE attnum > 0 AND attrelid IN ( WITH input AS ( SELECT CASE WHEN array_length(a, 1) = 2 THEN a ELSE ARRAY['public', a[1]] END AS s FROM (SELECT regexp_split_to_array('%s', '[.]')) AS t(a)) SELECT c.oid FROM pg_catalog.pg_class c LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace, input WHERE c.relname = input.s[2] AND n.nspname = input.s[1]);", arg),
                        String.format(
                            "SELECT '\"' || cn.conname || '\" ' || pg_catalog.pg_get_constraintdef(cn.oid, true) as constraints FROM pg_catalog.pg_class AS cl JOIN pg_catalog.pg_constraint AS cn ON cl.oid = cn.conrelid WHERE cl.oid IN (WITH input AS (SELECT CASE WHEN array_length(a, 1) = 2 THEN a ELSE ARRAY['public', a[1]] END AS s FROM (SELECT regexp_split_to_array('%s', '[.]')) AS t(a)) SELECT c.oid FROM pg_catalog.pg_class c LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace, input WHERE c.relname = input.s[2] AND n.nspname = input.s[1]);", arg),
                        String.format(
                            "WITH input AS (SELECT CASE WHEN array_length(a, 1) = 2 THEN a ELSE ARRAY['public', a[1]] END AS s FROM (SELECT regexp_split_to_array('%s', '[.]')) AS t(a)) SELECT indexdef AS indecies FROM pg_indexes JOIN input ON (schemaname = s[1] AND tablename = s[2]);", arg));
                }
        });

        this.commands.put("dn", arg -> ExpandedQuery.command(
            "SELECT n.nspname AS \"Schema\", pg_catalog.pg_get_userbyid(n.nspowner) AS \"Owner\" FROM pg_catalog.pg_namespace n WHERE n.nspname !~ '^pg_' AND n.nspname <> 'information_schema' ORDER BY 1;"));

        this.commands.put("i", arg -> readQueryFromFile(arg));
    }

    public ExpandedQuery expandCommand(String command) {

        if (!command.startsWith("\\")) {
            if (dbContext != null) {
                return ExpandedQuery.expanded(translateWithLore(command));
            } else {
                return ExpandedQuery.expanded(command);
            }
        }

        String stripped = command.strip().substring(1);
        int firstWhitespace = stripped.indexOf(' ');
        int splitPos = firstWhitespace > 0 ? firstWhitespace : stripped.length();
        String commandName = stripped.substring(0, splitPos);
        String arg = stripped.substring(splitPos).strip();

        switch(commandName) {
            case "e":
                return ExpandedQuery.usermade(openExternalEditor(arg));
            default:
                return lookupInMap(commandName, arg);
        }
    }

    private String openExternalEditor(String query) {

        String newQuery = "";
        String command = this.externalEditor + " <filename>";

        try {
            Path path = Files.createTempFile(TMP_FILENAME, ".sql");
            Files.writeString(path, query);
    
            command = this.externalEditor + " " + path.toString();
            Runtime.getRuntime().exec(command).waitFor();

            newQuery = Files.readString(path);
        } catch (IOException | InterruptedException ex) {
            System.err.println("[ERROR] Error when attempting to open external editor: " + ex.getMessage() + " (using command '" + command + "')");
        }

        return newQuery;
    }

    private ExpandedQuery lookupInMap(String commandName, String arg) {
        if (this.commands.containsKey(commandName)) {
            return this.commands.get(commandName).apply(arg);
        } else {
            return ExpandedQuery.expanded("SELECT 'Unknown command: \\" + commandName + "' AS \"ERROR\";");
        }
    }

    private String translateWithLore(String command) {
        
        List<String> sqlStmts = Lore.translateToSQL(command, this.dbContext);
        return sqlStmts.stream().collect(Collectors.joining(""));
    }

    private static ExpandedQuery readQueryFromFile(String filename) {
        
        String script;

        try {
            script = Files.readString(Paths.get(filename));
        } catch (IOException ex) {
             script = "SELECT '" + ex.getMessage() + "' AS \"ERROR\";";
        } 

        return ExpandedQuery.command(script);
    }
}
