package xyz.edbit;

import java.util.Arrays;
import java.util.List;

public class ExpandedQuery {

    enum Type { USERMADE, COMMAND, EXPANDED }

    private final List<String> queries; // Allow more than one
    private final Type type;

    private ExpandedQuery(Type type, String... queries) {
        this.type = type;
        this.queries = Arrays.asList(queries);
    }

    public static ExpandedQuery command(String... queries) {
        return new ExpandedQuery(Type.COMMAND, queries);
    }

    public static ExpandedQuery usermade(String... queries) {
        return new ExpandedQuery(Type.USERMADE, queries);
    }

    public static ExpandedQuery expanded(String... queries) {
        return new ExpandedQuery(Type.EXPANDED, queries);
    }

    public List<String> getQueries() {
        return this.queries;
    }

    public boolean isCommandQuery() {
        return this.type == Type.COMMAND;
    }

    public boolean isUsermadeQuery() {
        return this.type == Type.USERMADE;
    }

    public boolean isExpandedQuery() {
        return this.type == Type.EXPANDED;
    }

    public String toSingleQuery() {
        return String.join(";\n", this.queries);
    }
}
