package xyz.edbit;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import org.leifhka.lore.DatabaseContext;

import picocli.CommandLine;
import picocli.CommandLine.ParameterException;

public class Edbit extends Application {

    private static HistorySqlCodeArea history;
    private static SqlCodeArea current;
    private static Connection connection;
    private static Session session;
    private static Configuration configuration;
    private static MetaDatabase metaDatabase;
    private static Settings settings;

    public static void main(String[] args) {

        settings = new Settings();
        CommandLine cli = new CommandLine(settings);
        try {
            cli.parse(args);
        } catch (ParameterException ex) {
            System.err.println(ex.getMessage());
            return;
        }

        if (cli.isUsageHelpRequested()) {
            cli.usage(System.out);
            return;
        }

        if (cli.isVersionHelpRequested()) {
            cli.printVersionHelp(System.out);
            return;
        }

        try {

            connection = connect();
            DatabaseContext dbContext = settings.loreMode
                ? new DatabaseContext(connection)
                : null;

            configuration = new Configuration(dbContext, settings.external);
            metaDatabase = new MetaDatabase();
            metaDatabase.initMetadata(connection);

        } catch (ClassNotFoundException|SQLException|JSchException ex) {
            System.err.println(ex.getMessage());
            return; 
        }

        Application.launch(args);

        close();
    }

    @Override
    public void start(Stage primaryStage) {

        primaryStage.getIcons().add(
                new Image(Edbit.class.getClassLoader().getResourceAsStream("edbit.png")));

        history = new HistorySqlCodeArea(metaDatabase);
        history.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

        current = new EditableSqlCodeArea(primaryStage, connection, configuration, metaDatabase, history, settings.debugMode);
        current.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

        InfoArea hostInfo = new InfoArea("Host", settings.host);
        hostInfo.setMaxSize(Double.MAX_VALUE, 35);

        InfoArea dbInfo = new InfoArea("Database", settings.database);
        dbInfo.setMaxSize(Double.MAX_VALUE, 35);

        InfoArea userInfo = new InfoArea("User", settings.username);
        userInfo.setMaxSize(Double.MAX_VALUE, 35);

        HBox info = new HBox(hostInfo, dbInfo, userInfo);
        info.setHgrow(hostInfo, Priority.SOMETIMES);
        info.setHgrow(dbInfo, Priority.SOMETIMES);
        info.setHgrow(userInfo, Priority.SOMETIMES);

        VBox vbox = new VBox(history, current, info);
        vbox.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        vbox.setFillWidth(true);
        vbox.setVgrow(current, Priority.SOMETIMES);
        vbox.setVgrow(history, Priority.SOMETIMES);
        vbox.setVgrow(info, Priority.NEVER);

        Scene scene = new Scene(vbox, 800, 500);
        scene.getStylesheets()
            .add(SqlCodeArea.class.getClassLoader()
                            .getResource("style.css")
                            .toExternalForm());

        primaryStage.setTitle("Edbit");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    
    private static Connection connect() throws ClassNotFoundException, SQLException, JSchException {

        int port = 5432;

        if (settings.sshHost != null) {
            port = connectViaSSH();
        }

        Class.forName("org.postgresql.Driver");
        // Create a connection to the database
        java.io.Console console = System.console();
        String password = new String(console.readPassword("Password for database " + settings.database + " for user " + settings.username + ": ")); 
        String connectionStr = "user=" + settings.username;
        if (!password.equals("")) {
            connectionStr += "&password=" + password;
        }
        String host = settings.sshHost == null ? settings.host : "localhost";
        Connection connection = DriverManager.getConnection("jdbc:postgresql://" + host + ":" + port + "/" + settings.database + "?" + connectionStr);
        System.out.println("Connected to database " + settings.database + " with user " + settings.username + " on host " + settings.host + ".");
        return connection;
    }

    private static int connectViaSSH() throws JSchException {

        if (settings.sshUser == null) {
            settings.sshUser = settings.username;
        }

        java.io.Console console = System.console();
        String password = new String(console.readPassword("Password for user " + settings.sshUser + " at " + settings.sshHost + ": ")); 

        JSch jsch = new JSch();
        session = jsch.getSession(settings.sshUser, settings.sshHost);
        session.setConfig("StrictHostKeyChecking", "no");

        if (password != null && !password.equals("")) {
            session.setPassword(password);
        }

        session.connect();
        System.out.println("Connected via SSH to " + settings.sshHost + " with user " + settings.sshUser + ".");
        return session.setPortForwardingL(0, settings.host, 5432);
    }

    private static void close() {
        try {
            if (connection != null) {
                connection.close();
                System.out.println("JDBC connection closed.");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        if (session != null) {
            session.disconnect();
            System.out.println("SSH session disconnected.");
        }
    }
}
