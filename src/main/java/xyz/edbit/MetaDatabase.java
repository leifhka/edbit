package xyz.edbit;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

class MetaDatabase {

    private List<String> allNames;

    public MetaDatabase() {}

    public void initMetadata(Connection connection) throws SQLException {

        this.allNames = new LinkedList<>();
        getSchemasFromDB(connection);
        getTablesFromDB(connection);
        getColumnsFromDB(connection);
        this.allNames.sort(null);
    }

    private void getSchemasFromDB(Connection connection) throws SQLException {

        ResultSet schemaRS = connection.getMetaData().getSchemas();
        while (schemaRS.next()) {
            this.allNames.add(schemaRS.getString("TABLE_SCHEM"));
        }
    }

    private void getTablesFromDB(Connection connection) throws SQLException {

        ResultSet tableRS = connection.getMetaData().getTables(null, null, null, null);

        while (tableRS.next()) {
            String schema = tableRS.getString("TABLE_SCHEM");
            String table = tableRS.getString("TABLE_NAME");
            this.allNames.add(table);
            this.allNames.add(schema + "." + table);
        }
    }

    private void getColumnsFromDB(Connection connection) throws SQLException {

        ResultSet columnRS = connection.getMetaData().getColumns(null, null, null, null);

        while (columnRS.next()) {
            String schema = columnRS.getString("TABLE_SCHEM");
            String table = columnRS.getString("TABLE_NAME");
            String column = columnRS.getString("COLUMN_NAME");
            this.allNames.add(column);
            this.allNames.add(table + "." + column);
            this.allNames.add(schema + "." + table + "." + column);
        }
    }

    public List<String> getAllNames() {
        return this.allNames;
    }
}
